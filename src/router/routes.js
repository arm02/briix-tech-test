const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: "home",
        component: () => import("pages/IndexPage.vue"),
      },
      {
        path: "movies",
        children: [
          {
            path: "",
            name: "movies",
            component: () => import("pages/Movies/MoviesPage.vue"),
          },
          {
            path: "form/:key",
            name: "form-movies",
            component: () => import("pages/Movies/FormMovies.vue"),
          },
        ],
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
