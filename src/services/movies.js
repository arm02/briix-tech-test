import http from "src/boot/api";

export default function moviesService() {
  const { get, post, put, destroy } = http("movies");

  return {
    get,
    post,
    put,
    destroy,
  };
}
