import { api } from "./axios";

export default function http(url) {
  const get = async (params) => {
    try {
      const { data } = await api.get(url + params);
      return data;
    } catch (error) {
      throw new Error(error);
    }
  };

  const post = async (payload) => {
    try {
      const { data } = await api.post(url, payload);
      return data;
    } catch (error) {
      throw new Error(error);
    }
  };

  const put = async (payload, params) => {
    try {
      const { data } = await api.put(url + params, payload);
      return data;
    } catch (error) {
      throw new Error(error);
    }
  };

  const destroy = async (params) => {
    try {
      const { data } = await api.delete(url + params);
      return data;
    } catch (error) {
      throw new Error(error);
    }
  };

  return {
    get,
    post,
    put,
    destroy,
  };
}
