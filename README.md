# BRIIX Test (briix-quasar-test)

For BRIIX Tech Requirement Test

## Install the dependencies

```bash
yarn
# or
npm install
npm install json-server -g
```

### Start the JSON Server for API Mock (Inside the directory root project)

```bash
json-server --watch db.json
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
